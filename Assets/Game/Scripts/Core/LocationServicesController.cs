﻿using PlayFab;
using PlayFab.ClientModels;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocationServicesController : Singleton<LocationServicesController>
{
    [HideInInspector]
    public float longitude;

    [HideInInspector]
    public float latitude;
    void Start()
    {
      
        StartCoroutine(LocationServiceUpdate());
    }

    IEnumerator LocationServiceUpdate()
    {
        Input.location.Start();

        int waitTime = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && waitTime > 0)
        {
            yield return new WaitForSeconds(1);
            waitTime--;
        }

        if (waitTime <= 0)
        {
            Debug.Log("Timed out");
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Failed to determine device location");
            yield break;
        }
        else
        {
            longitude = Input.location.lastData.longitude;
            latitude = Input.location.lastData.latitude;
        }

        Input.location.Stop();
    }

    public void SaveStats()
    {
        if (PokeAPI.instance.pokemon != null)
        {
            PlayFabClientAPI.WritePlayerEvent(
            new WriteClientPlayerEventRequest()
            {
                EventName = "player_catch_pokemon",
                Body = new Dictionary<string, object>() {
                    {
                        "pokemon_name", PokeAPI.Instance.pokemon.name
                    },
                    {
                        "latitude", latitude.ToString()
                    },
                    {
                        "longitude", longitude.ToString()
                    }
                }
            },
            result =>
            {
                Debug.Log("Success sending event: player_update_displayname");
            },
            error =>
            {
                Debug.Log("Failed to send event: player_update_displayname");
            }
        );
        }
    }
}
