﻿using System;
[System.Serializable]
public class Pokemon
{
    public int id;
    public string name;
    public PokeSprites sprites;

}
