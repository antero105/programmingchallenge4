﻿using System;

[Serializable]
public class PokeSprites
{
    public string back_default;
    public string back_shinny;
    public string front_default;
    public string front_shiny;
}
